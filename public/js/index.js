"use strict";
// Shorthand for $( document ).ready()
$(function () {
  console.log("ready!");
});
// VARIABLES
let primaryCounter = 1;
let pauseCheck = "OFF";
const numImages = 5;
let player1Score = 0;
let player2Score = 0;
let player3Score = 0;
let player4Score = 0;
let player1Name = "Jugador 1";
let player2Name = "Jugador 2";
let player3Name = "Jugador 3";
let player4Name = "Jugador 4";

// FUNCTIONS
const nextImage = function (movieID, i, imageSrc) {
  if (
    document.getElementById(movieID).src.includes(`${imageSrc}(${i}).jpg`) &&
    primaryCounter < numImages
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i + 1}).jpg`;
    primaryCounter++;
  } else if (
    document.getElementById(movieID).src.includes(`${imageSrc}(${i}).jpeg`) &&
    primaryCounter < numImages
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i + 1}).jpeg`;
    primaryCounter++;
  }
};

const previousImage = function (movieID, i, imageSrc) {
  if (
    document.getElementById(movieID).src.includes(`${imageSrc}(${i}).jpg`) &&
    primaryCounter > 1
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i - 1}).jpg`;
    primaryCounter--;
  } else if (
    document.getElementById(movieID).src.includes(`${imageSrc}(${i}).jpeg`) &&
    primaryCounter > 1
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i - 1}).jpeg`;
    primaryCounter--;
  }
};

const roundStart = function (movieID, i, imageSrc) {
  if (
    primaryCounter < numImages &&
    pauseCheck === "OFF" &&
    document.getElementById(movieID).src.includes("LBT")
  ) {
    setTimeout(() => {
      document.getElementById(movieID).src = `${imageSrc}(${i + 1}).jpg`;
      primaryCounter++;
      roundStart("LBT", primaryCounter, "LBT/Land_Before_Time_characters");
    }, 5000);
  } else if (
    primaryCounter <= numImages &&
    pauseCheck === "ON" &&
    document.getElementById(movieID).src.includes("LBT")
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i - 1}).jpg`;
    primaryCounter--;
  } else if (
    primaryCounter < numImages &&
    pauseCheck === "OFF" &&
    document.getElementById(movieID).src.includes("Parasite")
  ) {
    setTimeout(() => {
      document.getElementById(movieID).src = `${imageSrc}(${i + 1}).jpeg`;
      primaryCounter++;
      roundStart("LBT", primaryCounter, "Parasite/Parasite_Clue");
    }, 5000);
  } else if (
    primaryCounter <= numImages &&
    pauseCheck === "ON" &&
    document.getElementById(movieID).src.includes("Parasite")
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i - 1}).jpeg`;
    primaryCounter--;
  } else if (
    primaryCounter < numImages &&
    pauseCheck === "OFF" &&
    document.getElementById(movieID).src.includes("Jaws")
  ) {
    setTimeout(() => {
      document.getElementById(movieID).src = `${imageSrc}(${i + 1}).jpg`;
      primaryCounter++;
      roundStart("LBT", primaryCounter, "Jaws/Jaws_Clue");
    }, 5000);
  } else if (
    primaryCounter <= numImages &&
    pauseCheck === "ON" &&
    document.getElementById(movieID).src.includes("Jaws")
  ) {
    document.getElementById(movieID).src = `${imageSrc}(${i - 1}).jpg`;
    primaryCounter--;
  }
};

const roundPause = function () {
  if (pauseCheck === "OFF") {
    pauseCheck = "ON";
  } else if (
    pauseCheck === "ON" &&
    document.getElementById("LBT").src.includes("LBT")
  ) {
    pauseCheck = "OFF";
    roundStart("LBT", primaryCounter, "LBT/Land_Before_Time_characters");
  } else if (
    pauseCheck === "ON" &&
    document.getElementById("LBT").src.includes("Parasite")
  ) {
    pauseCheck = "OFF";
    roundStart("LBT", primaryCounter, "Parasite/Parasite_Clue");
  } else if (
    pauseCheck === "ON" &&
    document.getElementById("LBT").src.includes("Jaws")
  ) {
    pauseCheck = "OFF";
    roundStart("LBT", primaryCounter, "Jaws/Jaws_Clue");
  }
};

const playerNames = function () {
  document.getElementById("player1Name").innerHTML = player1Name
    ? player1Name
    : "George";
  document.getElementById("player2Name").innerHTML = player2Name
    ? `${player2Name}`
    : "Kramer";
  document.getElementById("player3Name").innerHTML = player3Name
    ? `${player3Name}`
    : "Elaine";
  document.getElementById("player4Name").innerHTML = player4Name
    ? `${player4Name}`
    : "Jerry";
};
// BUTTONS
$(".buttonNext").click(function () {
  console.log("Next button clicked");
  nextImage("LBT", primaryCounter, "LBT/Land_Before_Time_characters");
  nextImage("LBT", primaryCounter, "Parasite/Parasite_Clue");
  nextImage("LBT", primaryCounter, "Jaws/Jaws_Clue");
});
$(".buttonPrevious").click(function () {
  console.log("Previous button clicked");
  previousImage("LBT", primaryCounter, "LBT/Land_Before_Time_characters");
  previousImage("LBT", primaryCounter, "Parasite/Parasite_Clue");
  previousImage("LBT", primaryCounter, "Jaws/Jaws_Clue");
});
$(".buttonStart").click(function () {
  primaryCounter = 1;
  pauseCheck = "OFF";
  if (document.getElementById("LBT").src.includes("LBT")) {
    document.getElementById(
      "LBT"
    ).src = `LBT/Land_Before_Time_characters(1).jpg`;
    roundStart("LBT", primaryCounter, "LBT/Land_Before_Time_characters");
  } else if (document.getElementById("LBT").src.includes("Parasite")) {
    document.getElementById("LBT").src = `Parasite/Parasite_Clue(1).jpeg`;
    roundStart("LBT", primaryCounter, "Parasite/Parasite_Clue");
  } else if (document.getElementById("LBT").src.includes("Jaws")) {
    document.getElementById("LBT").src = `Jaws/Jaws_Clue(1).jpg`;
    roundStart("LBT", primaryCounter, "Jaws/Jaws_Clue");
  }
  console.log("Round start button clicked");
});

$(".buttonPause").click(function () {
  if (pauseCheck === "OFF") {
    $(".buttonPause").css("background-color", "green");
    document.getElementById("btnPause").innerHTML = "Resume";
  } else if (pauseCheck === "ON") {
    $(".buttonPause").css("background-color", "red");
    document.getElementById("btnPause").innerHTML = "Pause";
  }
  console.log("Pause button clicked");
  roundPause();
});

$("#btnUndo1").click(function () {
  console.log("Undo button 1 clicked");
  if (player1Score >= 10) {
    player1Score -= 10;
    document.getElementById(
      "player1Score"
    ).innerHTML = `Score: ${player1Score}`;
  }
});
$("#btnUndo2").click(function () {
  console.log("Undo button 2 clicked");
  if (player2Score >= 10) {
    player2Score -= 10;
    document.getElementById(
      "player2Score"
    ).innerHTML = `Score: ${player2Score}`;
  }
});
$("#btnUndo3").click(function () {
  console.log("Undo button 3 clicked");
  if (player3Score >= 10) {
    player3Score -= 10;
    document.getElementById(
      "player3Score"
    ).innerHTML = `Score: ${player3Score}`;
  }
});
$("#btnUndo4").click(function () {
  console.log("Undo button 4 clicked");
  if (player4Score >= 10) {
    player4Score -= 10;
    document.getElementById(
      "player4Score"
    ).innerHTML = `Score: ${player4Score}`;
  }
});

// document.getElementById("playerForm").onsubmit = function () {
//   player1Name = document.getElementById("p1name").innerHTML;
//   console.log("Form submitted");
//   playerNames();
// };
const Boobertini = function () {
  player1Name = document.getElementById("p1name").value;
  player2Name = document.getElementById("p2name").value;
  player3Name = document.getElementById("p3name").value;
  player4Name = document.getElementById("p4name").value;
  playerNames();
  console.log("Form submitted");
};

// PLAYER SCORES
$(".scoreCard-1 img").click(function () {
  player1Score += 10;
  document.getElementById("player1Score").innerHTML = `Score: ${player1Score}`;
});
$(".scoreCard-2 img").click(function () {
  player2Score += 10;
  document.getElementById("player2Score").innerHTML = `Score: ${player2Score}`;
});
$(".scoreCard-3 img").click(function () {
  player3Score += 10;
  document.getElementById("player3Score").innerHTML = `Score: ${player3Score}`;
});
$(".scoreCard-4 img").click(function () {
  player4Score += 10;
  document.getElementById("player4Score").innerHTML = `Score: ${player4Score}`;
});
// Start up functions?
playerNames();

// Dropdown Question Selector
$("#qstNmbr").change(function () {
  if ($(this).val() === "question1") {
    primaryCounter = 1;
    pauseCheck = "OFF";
    $(".buttonPause").css("background-color", "red");
    document.getElementById("btnPause").innerHTML = "Pause";
    document.getElementById(
      "LBT"
    ).src = `LBT/Land_Before_Time_characters(1).jpg`;
    console.log("You've chosen Question 1");
  }
  if ($(this).val() === "question2") {
    primaryCounter = 1;
    pauseCheck = "OFF";
    $(".buttonPause").css("background-color", "red");
    document.getElementById("btnPause").innerHTML = "Pause";
    document.getElementById("LBT").src = "Parasite/Parasite_Clue(1).jpeg";
    console.log("You've chosen Question 2");
  }
  if ($(this).val() === "question3") {
    primaryCounter = 1;
    pauseCheck = "OFF";
    $(".buttonPause").css("background-color", "red");
    document.getElementById("btnPause").innerHTML = "Pause";
    document.getElementById("LBT").src = "Jaws/Jaws_Clue(1).jpg";
    console.log("You've chosen Question 3");
  }
  if ($(this).val() === "question4") {
    console.log("You've chosen Question 4");
  }
});
